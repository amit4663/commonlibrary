package com.narayan.app.response;

public class ApiResponseDto<T> implements ResponseDto<T> {

    private int code;
    private T data;
    private String message;

    @Override
    public int getCode() {
        return code;
    }

    @Override

    public void setCode(int code) {
        this.code = code;
    }

    @Override
    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }

    public ApiResponseDto() {
    }

    public ApiResponseDto(int code, T data, String message) {
        this.code = code;
        this.data = data;
        this.message = message;
    }
}
