package com.narayan.app.response;

public enum ApiResponse implements ResponseCode {

    SUCCESS(0,"SUCCESS");

    private int code;
    private String message;

    private ApiResponse(int code,String message){
        this.code = code;
        this.message = message;
    }

    @Override
    public int code() {
       return  this.code;
    }

    @Override
    public String message() {
        return this.message;
    }
}
