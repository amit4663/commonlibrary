package com.narayan.app.response;

public interface ResponseCode {
    int code();

    String message();
}
