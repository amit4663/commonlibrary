package com.narayan.app.response;

public interface ResponseDto<T> {
    int getCode();

    void setCode(int code);

    String getMessage();

    void setMessage(String message);

    void setData(T data);

    T getData();

}
