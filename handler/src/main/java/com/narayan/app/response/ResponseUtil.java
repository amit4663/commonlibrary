package com.narayan.app.response;

import com.narayan.app.handler.ValidationErrorResponse;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class ResponseUtil {
    private static final Map data = new HashMap();

    public ResponseDto ok(Object data,ResponseCode responseCode){
        return new ApiResponseDto(responseCode.code(),data,responseCode.message());
    }
    public ResponseDto ok(ResponseCode responseCode){
        return new ApiResponseDto(responseCode.code(),data,responseCode.message());
    }
    public ResponseDto validateException(int code, ValidationErrorResponse errorResponse){
        return new ApiResponseDto(code,errorResponse.getErrorMessages(),"Missing Parameters");
    }

}
