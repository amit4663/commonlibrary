package com.narayan.app.handler;

import com.narayan.app.constants.HttpStatus;
import com.narayan.app.exceptions.ValidationException;
import com.narayan.app.response.ResponseDto;
import com.narayan.app.response.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

@ControllerAdvice(basePackages = "com.narayan.app")
@RestController
public class ApiExceptionHandler {
    @Autowired
    private ResponseUtil responseUtil;

    @ExceptionHandler(ValidationException.class)
    public ResponseDto handleGenericException(ValidationException e){
        ValidationErrorResponse errorResponse = new ValidationErrorResponse();
        errorResponse.addMessage(e.getMessage());
        return responseUtil.validateException(HttpStatus.BAD_REQUEST.getCode(),errorResponse);
    }
}
