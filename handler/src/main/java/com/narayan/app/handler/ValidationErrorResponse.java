package com.narayan.app.handler;

import java.util.List;

public class ValidationErrorResponse {
    private List<String> errorMessages;

    public List<String> getErrorMessages() {
        return errorMessages;
    }

    public void setErrorMessages(List<String> errorMessages) {
        this.errorMessages = errorMessages;
    }
    public void addMessage(String message){
        this.errorMessages.add(message);
    }

}
