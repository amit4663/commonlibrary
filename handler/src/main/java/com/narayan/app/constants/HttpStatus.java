package com.narayan.app.constants;

public enum  HttpStatus {

    BAD_REQUEST(400,"Bad Request");

    private int code;
    private String message;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private HttpStatus(int code, String  message){
        this.code = code;
        this.message = message;
    }

}
